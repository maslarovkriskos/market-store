﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market_Store
{
    public class SilverCard : Card
    {
        public override string DiscountByCard(double turnOver, double purchaseValue)
        {
            var sb = new StringBuilder();

            this.DiscountRate = 0.02;

            if (turnOver > 300)
            {
                this.DiscountRate = 0.035;
            }

            this.PurchaseValue = purchaseValue;
            this.Total = PurchaseValue - (PurchaseValue * this.DiscountRate);
            this.Discount = PurchaseValue - this.Total;
            this.DiscountRate *= 100;

            sb.AppendLine($"Purchase value: ${this.PurchaseValue:f2}");
            sb.AppendLine($"Discount rate: {this.DiscountRate:f1}%");
            sb.AppendLine($"Discount: ${this.Discount:f2}");
            sb.AppendLine($"Total: ${this.Total:f2}");

            return sb.ToString();
        }
    }
}