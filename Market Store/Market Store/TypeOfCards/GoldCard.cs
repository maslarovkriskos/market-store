﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market_Store
{
    public class GoldCard : Card
    {
        public override string DiscountByCard(double turnOver, double purchaseValue)
        {
            var sb = new StringBuilder();

            this.DiscountRate = 0.02;

            if (turnOver >= 100 && turnOver < 800)
            {
                if (turnOver >= 100 && turnOver < 200)
                {
                    this.DiscountRate += 0.01;
                }
                else if (turnOver >= 200 && turnOver < 300)
                {
                    this.DiscountRate += 0.02;
                }
                else if (turnOver >= 300 && turnOver < 400)
                {
                    this.DiscountRate += 0.03;
                }
                else if (turnOver >= 400 && turnOver < 500)
                {
                    this.DiscountRate += 0.04;
                }
                else if (turnOver >= 500 && turnOver < 600)
                {
                    this.DiscountRate += 0.05;
                }
                else if (turnOver >= 600 && turnOver < 700)
                {
                    this.DiscountRate += 0.06;
                }
                else if (turnOver >= 700 && turnOver < 800)
                {
                    this.DiscountRate += 0.07;
                }
            }
            else if (turnOver >= 800)
            {
                this.DiscountRate += 0.08;
            }

            this.PurchaseValue = purchaseValue;
            this.Total = PurchaseValue - (PurchaseValue * this.DiscountRate);
            this.Discount = PurchaseValue - this.Total;
            this.DiscountRate *= 100;

            sb.AppendLine($"Purchase value: ${this.PurchaseValue:f2}");
            sb.AppendLine($"Discount rate: {this.DiscountRate:f1}%");
            sb.AppendLine($"Discount: ${this.Discount:f2}");
            sb.AppendLine($"Total: ${this.Total:f2}");

            return sb.ToString();
        }
    }
}
